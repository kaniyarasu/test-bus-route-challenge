# Bus Route Challenge

### Assumptions

* In bus route, stations are connected in the one direction(forward)
* On validation failure, application won't start
* On configuration change, server need to be restarted
* In most case, code itself is self explanatory (Preferred minimal comments)
* Localised error message are not used

### Design

* Command Action design pattern used, which mean separate action class for every command. Instead of having all the action in single Service, we will be having separate Action class for every functionality 
  * For eg. Search should have separate Action class, and create should have separate Action class. 
  * More details https://martinfowler.com/bliki/CQRS.html
  
* Data structure
  * Used Inverted index on station ids, reason could be to search on station ids.
    * HashMap<Integer, HashSet<Integer>> -> HashMap<DepartureStationId, HashSet<ArrivalStationId>>
  * Created index on departure station and then added connecting station ids.
  * Complexity on search could be O(1) for finding departure station and check arrival station is O(1)
  * Haven't included the routes in storage, for current requirement we don't need this. 

### Configuration

* Configuration can be either added in application.properties or overwritten with command line args.

### Tech stack

* Spring Boot
* Tomcat
* Spring Boot Test/JUnit/Mockito
* Slf4j
* Open source libraries(Lombok/Commons-collection/Commons-io)

### Example

Bus Routes Data File:
```
3
0 0 1 2 3 4
1 3 1 6 5
2 0 6 4
```

Query:
```
http://localhost:8088/api/direct?dep_sid=3&arr_sid=6
```

Response:
```
{
    "dep_sid": 3,
    "arr_sid": 6,
    "direct_bus_route": true
}
```


### Packaging

Run `./service.sh` to see usage. For a starter:

```
./service.sh           : prints usage
./service.sh dev_build : builds your app
./service.sh dev_run   : runs your app
```

If you have `docker` installed on your machine, you can also run all the
scripts and tests inside a docker container:

```
./service.sh docker_build : packages your app into a docker image
./service.sh docker_run   : runs your app using a docker image
./service.sh docker_smoke : runs same smoke tests inside a docker container
```

