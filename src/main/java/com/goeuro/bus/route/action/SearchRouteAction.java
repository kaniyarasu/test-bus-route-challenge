package com.goeuro.bus.route.action;

import com.goeuro.bus.route.model.RouteResponse;
import com.goeuro.bus.route.model.RouteSearchCriteria;
import com.goeuro.bus.route.repository.RouteDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Action to search for route existence
 */
@Component
@Slf4j
public class SearchRouteAction
{

    @Autowired
    private RouteDataRepository routeDataRepository;

    public RouteResponse invoke(RouteSearchCriteria routeSearchCriteria) {
        log.info("Inside the get route action with request {}", routeSearchCriteria);

        boolean routeExist = routeDataRepository.isRouteExist(routeSearchCriteria);
        log.info(routeExist ? "Route search - hit for the request {}"
                        : "Route search - miss for the request {}",
                routeSearchCriteria);

        return new RouteResponse(routeSearchCriteria.getDepartureStationId(),
                routeSearchCriteria.getArrivalStationId(),
                routeExist);
    }
}
