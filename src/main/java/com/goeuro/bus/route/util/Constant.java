package com.goeuro.bus.route.util;

public interface Constant {

    String EC_MAX_ROUTE_STATION_LIMIT = "BRS001";
    String EC_MAX_STATION_LIMIT = "BRS002";
    String EC_MAX_ROUTE_LIMIT = "BRS001";

    String DATA_SEPARATOR = "\\s+";
}
