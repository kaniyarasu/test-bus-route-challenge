package com.goeuro.bus.route.repository;

import com.goeuro.bus.route.helper.RouteDataManager;
import com.goeuro.bus.route.model.RouteSearchCriteria;
import com.goeuro.bus.route.model.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.IntStream;

@Component
public class RouteDataRepository {

    @Value("${data.file.path:data/sample}")
    private String dataFilePath;

    @Autowired
    private RouteDataManager routeDataManager;

    private Map<Integer, Set<Integer>> availableConnections = new HashMap<>();

    @PostConstruct
    private void init() {
        //1. Get all routes
        Routes routes = routeDataManager.invoke(dataFilePath);

        //2. Constructs inverted index on station Id and all connecting station Ids
        constructAvailableConnections(routes);
    }

    private void constructAvailableConnections(Routes routes)
    {
        routes.getRoutes()
                .forEach(route -> IntStream.range(0, route.getStations().size() - 1)
                        .forEach(index -> {
                            Integer currentStation = route.getStations().get(index);
                            if (!availableConnections.containsKey(currentStation))
                            {
                                availableConnections.put(currentStation, new HashSet<>());
                            }
                            availableConnections.get(currentStation)
                                    .addAll(route.getStations().subList(index + 1, route.getStations().size()));
                        }));
    }

    /**
     * Return true if routes exist between station irrespective of direction
     * @param routeSearchCriteria - Search parameters
     * @return boolean
     */
    public boolean isRouteExist(RouteSearchCriteria routeSearchCriteria) {
        return availableConnections.containsKey(routeSearchCriteria.getDepartureStationId())
                && availableConnections.get(routeSearchCriteria.getDepartureStationId())
                .contains(routeSearchCriteria.getArrivalStationId());
    }
}
