package com.goeuro.bus.route;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot starter
 */
@SpringBootApplication
public class BusRouteChallengeApplication {
    public static void main(String[] args) {
        SpringApplication.run(BusRouteChallengeApplication.class, args);
    }
}
