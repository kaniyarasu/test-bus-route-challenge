package com.goeuro.bus.route.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType {

    FILE_READ_ERROR("EC_0"),
    ROUTES_COUNT_MISMATCH("EC_1"),
    ROUTES_NOT_IN_ALLOWED_RANGE("EC_2"),
    DUPLICATE_ROUTE_FOUND("EC_3"),
    STATION_PER_ROUTES_NOT_IN_ALLOWED_RANGE("EC_4"),
    DUPLICATE_STATION_IN_ROUTE("EC_5"),
    EXCEEDS_MAX_UNIQUE_STATION_LIMIT("EC_6");

    private String value;
}
