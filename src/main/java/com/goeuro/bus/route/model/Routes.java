package com.goeuro.bus.route.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Routes
{
    private int numberOfRoutes;

    private List<Route> routes = new ArrayList<>();
}
