package com.goeuro.bus.route.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Route {
    private int routeId;

    private List<Integer> stations = new ArrayList<>();
}
