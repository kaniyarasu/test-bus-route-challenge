package com.goeuro.bus.route.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouteResponse {
    @JsonProperty("dep_sid")
    private int departureStationId;

    @JsonProperty("arr_sid")
    private int arrivalStationId;

    @JsonProperty("direct_bus_route")
    private boolean directBusRoute;
}
