package com.goeuro.bus.route.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RouteSearchCriteria {
    private int departureStationId;

    private int arrivalStationId;
}
