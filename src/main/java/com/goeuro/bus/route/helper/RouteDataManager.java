package com.goeuro.bus.route.helper;

import com.goeuro.bus.route.exception.InvalidDataException;
import com.goeuro.bus.route.model.Route;
import com.goeuro.bus.route.model.Routes;
import com.goeuro.bus.route.util.Constant;
import com.goeuro.bus.route.exception.ErrorType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class RouteDataManager
{

    @Value("${max.route.limit:100000}")
    private int maxRouteLimit;

    @Value("${max.station.limit:1000000}")
    private int maxStationLimit;

    @Value("${max.route.station.limit:1000}")
    private int maxRouteStationLimit;

    /**
     * This action perform three operations
     * 1. Read the records from file
     * 2. Convert the record to routes
     * 3. Validate the routes
     *
     * @return If extracted routes are valid, then returns Routes
     * @param dataFilePath
     */
    public Routes invoke(String dataFilePath) {
        // 1. Load the records from file
        List<String> records = loadRoutesFromFile(dataFilePath);

        //2. Extract routes from file records
        Routes routes = extractRoutes(records);

        //3. Validate routes
        validateRoutes(routes);

        return routes;
    }

    /**
     * Validates the extracted routes and filter the invalid routes.
     * @param routes - extracted routes
     */
    private void validateRoutes(Routes routes) {
        validateRouteCounts(routes);
        validateRouteLimit(routes);
        validateRouteDuplication(routes);

        validateStationLimitPerRoute(routes);
        validateStationDuplication(routes);
        validateMaximumUniqueStationIds(routes);
    }

    /**
     * Validates maximum allowed unique station Ids
     * @param routes - All routes
     */
    private void validateMaximumUniqueStationIds(Routes routes)
    {
        Set<Integer> uniqueStationIds = routes.getRoutes()
                .stream()
                .flatMap(route -> route.getStations().stream())
                .collect(Collectors.toSet());

        if(uniqueStationIds.size() > maxStationLimit){
            throw new InvalidDataException(ErrorType.EXCEEDS_MAX_UNIQUE_STATION_LIMIT,
                    uniqueStationIds.size(),
                    maxStationLimit);
        }
    }

    /**
     * Validates for duplicates in station in route
     * @param routes - All routes
     */
    private void validateStationDuplication(Routes routes)
    {
        routes.getRoutes().parallelStream()
                .forEach(route -> {
                    boolean isAllUnique = route.getStations()
                            .stream()
                            .allMatch(new HashSet<Integer>()::add);

                    if(!isAllUnique){
                        throw new InvalidDataException(ErrorType.DUPLICATE_STATION_IN_ROUTE);
                    }
                });
    }

    /**
     * Validates stations limit per routes
     * @param routes - All routes
     */
    private void validateStationLimitPerRoute(Routes routes)
    {
        routes.getRoutes().parallelStream()
                .forEach(route -> {
                    if (CollectionUtils.isEmpty(route.getStations())
                            || route.getStations().size() < 2
                            || route.getStations().size() > maxRouteStationLimit) {
                        throw new InvalidDataException(ErrorType.STATION_PER_ROUTES_NOT_IN_ALLOWED_RANGE,
                                route.getStations().size(),
                                2,
                                maxRouteStationLimit);
                    }
                });
    }

    /**
     * Validates for route duplication
     * @param routes - All routes
     */
    private void validateRouteDuplication(Routes routes)
    {
        Set<Integer> uniqueRouteIds = routes.getRoutes()
                .stream()
                .map(Route::getRouteId)
                .collect(Collectors.toSet());

        if(routes.getNumberOfRoutes() != uniqueRouteIds.size()){
            throw new InvalidDataException(ErrorType.DUPLICATE_ROUTE_FOUND);
        }
    }

    /**
     * Validates allowed route limit
     * @param routes - All routes
     */
    private void validateRouteLimit(Routes routes)
    {
        if(routes.getNumberOfRoutes() <= 0
                || routes.getNumberOfRoutes() > maxRouteLimit){
            throw new InvalidDataException(ErrorType.ROUTES_NOT_IN_ALLOWED_RANGE,
                    routes.getNumberOfRoutes(),
                    maxRouteLimit);
        }
    }

    /**
     * Validates given and actual route count
     * @param routes - All routes
     */
    private void validateRouteCounts(Routes routes)
    {
        if(routes.getNumberOfRoutes() != routes.getRoutes().size()){
            throw new InvalidDataException(ErrorType.ROUTES_COUNT_MISMATCH,
                    routes.getNumberOfRoutes(),
                    routes.getRoutes().size());
        }
    }

    /**
     * Extracts the route ids and station ids from file records
     * @param records - complete records from file
     * @return Routes
     */
    private Routes extractRoutes(List<String> records) {
        //1. Extracts the number of routes
        Integer numberOfRoutes = records.stream().findFirst().map(Integer::valueOf).get();

        //2. Extracts all routes along with stations
        List<Route> extractedRoutes = records.stream()
                .skip(1)
                .map(String::trim)
                .map(this::extractRoute)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        return new Routes(numberOfRoutes, extractedRoutes);
    }

    /**
     * Extract and construct the Route from file record
     * @param record - individual route record
     * @return Route
     */
    private Route extractRoute(String record) {
        //Extracts route id
        Integer routeId = Arrays.stream(record.split(Constant.DATA_SEPARATOR)).findFirst().map(Integer::valueOf).get();

        //Extracts stations
        List<Integer> stations = Arrays.stream(record.split(Constant.DATA_SEPARATOR))
                .skip(1)
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        return new Route(routeId, stations);
    }

    /**
     * Load data from file if found and readable
     * @param filePath - data file path
     * @return List of routes records
     */
    private List<String> loadRoutesFromFile(String filePath)
    {
        List<String> records;
        try{
            records = FileUtils.readLines(new File(filePath), Charset.defaultCharset());
        } catch (Exception e) {
            log.error("Unable to load file because of error {}", e.getMessage());
            throw new InvalidDataException(ErrorType.FILE_READ_ERROR, e.getMessage());
        }
        return records;
    }
}
