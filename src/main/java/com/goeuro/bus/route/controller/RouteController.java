package com.goeuro.bus.route.controller;

import com.goeuro.bus.route.action.SearchRouteAction;
import com.goeuro.bus.route.model.RouteResponse;
import com.goeuro.bus.route.model.RouteSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api", produces = "application/json")
public class RouteController {

    @Autowired
    private SearchRouteAction searchRouteAction;

    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/direct", method = RequestMethod.GET)
    public RouteResponse getRoute(@RequestParam("dep_sid") int departureStationId,
                                  @RequestParam("arr_sid") int arrivalStationId) {
        return searchRouteAction.invoke(new RouteSearchCriteria(departureStationId, arrivalStationId));
    }
}