package com.goeuro.bus.route.action;

import com.goeuro.bus.route.model.RouteResponse;
import com.goeuro.bus.route.model.RouteSearchCriteria;
import com.goeuro.bus.route.repository.RouteDataRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = {"data.file.path=src/test/resources/test-data/simple"})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class SearchRouteActionTest
{
    @InjectMocks
    private SearchRouteAction searchRouteAction;

    @Mock
    private RouteDataRepository routeDataRepository;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testResponse() {
        RouteResponse routeResponse = searchRouteAction.invoke(new RouteSearchCriteria(1, 2));

        assertThat(routeResponse.getDepartureStationId()).isEqualTo(1);
        assertThat(routeResponse.getArrivalStationId()).isEqualTo(2);
        assertThat(routeResponse.isDirectBusRoute()).isEqualTo(false);
    }
}