package com.goeuro.bus.route.repository;

import com.goeuro.bus.route.helper.RouteDataManager;
import com.goeuro.bus.route.model.Route;
import com.goeuro.bus.route.model.RouteSearchCriteria;
import com.goeuro.bus.route.model.Routes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.HashMap;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RouteDataRepositoryTest
{
    @InjectMocks
    RouteDataRepository routeDataRepository;

    @Mock
    RouteDataManager routeDataManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(routeDataRepository, "availableConnections", new HashMap<>());
        ReflectionTestUtils.invokeMethod(routeDataRepository, "constructAvailableConnections", prepareRoutes());
    }

    @Test
    public void testNoRoute() {
        assertFalse(routeDataRepository.isRouteExist(new RouteSearchCriteria(1, 21)));
    }

    @Test
    public void testAvailableRoute() {
        assertTrue(routeDataRepository.isRouteExist(new RouteSearchCriteria(2, 3)));
    }

    @Test
    public void testRouteInReverse() {
        assertFalse(routeDataRepository.isRouteExist(new RouteSearchCriteria(3, 2)));
    }

    private Routes prepareRoutes(){
        Route route = new Route(1, Arrays.asList(1, 2, 3, 4));
        return new Routes(1, Arrays.asList(route));
    }
}