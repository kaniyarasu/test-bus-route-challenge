package com.goeuro.bus.route.helper;

import com.goeuro.bus.route.exception.ErrorType;
import com.goeuro.bus.route.exception.InvalidDataException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RouteDataManagerTest {
    private static final String TEST_DATA_DIRECTORY = "src/test/resources/test-data/";

    @InjectMocks
    private RouteDataManager routeDataManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(routeDataManager, "maxRouteLimit", 12);
        ReflectionTestUtils.setField(routeDataManager, "maxStationLimit", 10);
        ReflectionTestUtils.setField(routeDataManager, "maxRouteStationLimit", 15);
    }

    /**
     * Test file not exists
     * @throws InvalidDataException
     */
    @Test
    public void testDataFileNotExists() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_0");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.FILE_READ_ERROR);
        }
    }

    /**
     * Test route count mismatch between given and extracted
     * @throws InvalidDataException
     */
    @Test
    public void testRouteCountMismatch() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_1");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.ROUTES_COUNT_MISMATCH);
        }
    }

    /**
     * Test route limits
     * @throws InvalidDataException
     */
    @Test
    public void testRouteLimit() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_2");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.ROUTES_NOT_IN_ALLOWED_RANGE);
        }
    }

    /**
     * Test route duplication
     * @throws InvalidDataException
     */
    @Test
    public void testRouteDuplication() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_3");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.DUPLICATE_ROUTE_FOUND);
        }
    }

    /**
     * Test station limit per route
     * @throws InvalidDataException
     */
    @Test
    public void testStationMinLimitPerRoute() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_4_1");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.STATION_PER_ROUTES_NOT_IN_ALLOWED_RANGE);
        }
    }

    /**
     * Test station limit per route
     * @throws InvalidDataException
     */
    @Test
    public void testStationMaxLimitPerRoute() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_4_2");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.STATION_PER_ROUTES_NOT_IN_ALLOWED_RANGE);
        }
    }

    /**
     * Test station duplication per route
     * @throws InvalidDataException
     */
    @Test
    public void testDuplicateStationPerRoute() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_5");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.DUPLICATE_STATION_IN_ROUTE);
        }
    }

    /**
     * Test maximum allowed unique stations
     * @throws InvalidDataException
     */
    @Test
    public void testMaximumAllowedUniqueStations() {
        try{
            routeDataManager.invoke(TEST_DATA_DIRECTORY + "EC_6");
        } catch (InvalidDataException e){
            assert(e.getErrorType()).equals(ErrorType.EXCEEDS_MAX_UNIQUE_STATION_LIMIT);
        }
    }
}