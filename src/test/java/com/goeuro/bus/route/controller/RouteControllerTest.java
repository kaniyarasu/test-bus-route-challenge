package com.goeuro.bus.route.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.goeuro.bus.route.action.SearchRouteAction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(properties = {"data.file.path=src/test/resources/test-data/simple"})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class RouteControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchRouteAction searchRouteAction;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testNoParameters() throws Exception {
        this.mockMvc.perform(get("/api/direct"))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Required int parameter 'dep_sid' is not present"));
    }

    @Test
    public void testEmptyParameter() throws Exception {
        this.mockMvc.perform(get("/api/direct").param("arr_sid", "").param("dep_sid", ""))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testWrongParameter() throws Exception {
        this.mockMvc.perform(get("/api/direct").param("arr_sid", "T").param("dep_sid", "T"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testNoDepartureSid() throws Exception {
        this.mockMvc.perform(get("/api/direct").param("arr_sid", String.valueOf(1)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Required int parameter 'dep_sid' is not present"));
    }

    @Test
    public void testNoArrivalSid() throws Exception {
        this.mockMvc.perform(get("/api/direct").param("dep_sid", String.valueOf(1)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Required int parameter 'arr_sid' is not present"));
    }

    @Test
    public void testDirectRoute() throws Exception
    {
        this.mockMvc
                .perform(
                        get("/api/direct")
                                .param("dep_sid", String.valueOf(142))
                                .param("arr_sid", String.valueOf(106)))
                .andExpect(status().isOk());
    }
}